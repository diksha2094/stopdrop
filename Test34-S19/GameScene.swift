//
//  GameScene.swift
//  Test34-S19
//
//  Created by MacStudent on 2019-06-19.
//  Copyright © 2019 rabbit. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene,SKPhysicsContactDelegate {
    
    var nextLevelButton:SKLabelNode!
    var xd:CGFloat = 0
    var yd:CGFloat = 0
     let square = SKSpriteNode(color: SKColor.red,size: CGSize(width: 40, height: 40))
    
    
    override func didMove(to view: SKView) {
       
            self.physicsWorld.contactDelegate = self
      
        square.position = CGPoint(x: 605, y:625)
     addChild(square)
                self.physicsWorld.contactDelegate = self
        print("This is level 1")
        self.nextLevelButton = self.childNode(withName: "nextLevelButton") as! SKLabelNode
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first

        let location = touch!.location(in:self)
        let node = self.atPoint(location)
        
        
        if (touch == nil) {
            return
        }
        else{
            let sq=SKSpriteNode(color: SKColor.blue,size: CGSize(width: 100, height: 40))
            sq.zPosition=99
            sq.position.x=node.position.x
            sq.position.y=node.position.y
            
            print(sq.position.x,sq.position.y)
            addChild(sq)
        }
        
        
        
        
        
        // MARK: Switch Levels
        if (node.name == "nextLevelButton") {
            let scene = SKScene(fileNamed:"Level2")
            if (scene == nil) {
                print("Error loading level")
                return
            }
            else {
                scene!.scaleMode = .aspectFill
                view?.presentScene(scene!)
            }
        }
        
    }
}
